# Backcrypt

## Description
Système de sauvegarde de fichier avec chiffrement, géré par fichier de configuration.

## Fonctionnalitées
- Compression en tar.bz2
- Chiffrement/Déchiffrement aes256
- Stockage en local
- Transfert via SSH
- Transfert via FTP
- Transfert vers un stockage S3
- Barre de progression pour la compression/chiffrement et transfert sur les différents types
- Script d'installation (bêta)
- Rétention des sauvegardes

## Reste à faire
- Logs
- Système de notification de sauvegarde
- Transfert vers un stockage NFS
- Sauvegarde par mot de passe

## Commandes disponible
```
usage: backcrypt [-h] [-l] [-t TEST_CONFIG] [-v] [-a] [-s SAVE [SAVE ...]] [-d DECRYPT_FILE] [-k KEY_FILE]

Système de sauvegarde avec chiffrement

options:
  -h, --help            show this help message and exit
  -l, --list-config     Liste les fichiers de configuration des sauvegardes
  -t TEST_CONFIG, --test-config TEST_CONFIG
                        Vérification de la configuration d'un fichier de sauvegarde
  -v, --version         Affiche la version de Backcrypt

Sauvegardes:
  -a, --save-all        Lance toutes les tâches de sauvegardes
  -s SAVE [SAVE ...], --save SAVE [SAVE ...]
                        Lance une tâche de sauvegarde

Restauration dune sauvegardes:
  -d DECRYPT_FILE, --decrypt-file DECRYPT_FILE
                        Déchiffre une sauvegarde
  -k KEY_FILE, --key-file KEY_FILE
                        Fichier de la clé de sauvegarde
```

## Exemple de configuration 

### Sauvegarde locale
```yaml
---

backup:
  name: SauvegardeLocale
  filename: SauvegardeLocale_%d-%m-%Y
  path:
    - "/var/www/html/site"
    - "/home/user/data/"
  dest: 
    type: host
    path: "/data/backup/"
  encryption:
    key_file: sauvegarde_locale.key
  retention: 3
```

### Sauvegarde en SSH (sans clé SSH)
```yaml
---

backup:
  name: DataUserSave
  filename: DataUserSave_%d-%m-%Y
  path: "/home/user/data/"
  force: no
  dest: 
    type: ssh
    path: "/data/backup/"
    login:
      user: save_user
      password: P@ssw0rd1
    hostname: myserver  
  encryption:
    key_file: cle_data_user.key
```

### Sauvegarde en SSH (avec clé SSH)
```yaml
---

backup:
  name: DataUserSave
  filename: DataUserSave_%d-%m-%Y
  path: "/home/user/data/"
  force: yes
  dest: 
    type: ssh
    path: "/data/backup/"
    login:
      user: save_user
      password: P@ssw0rd1
    hostname: myserver
    port: 2222
    ssh_key_file: ~/.ssh/id_ed25519
  encryption:
    key_file: cle_data_user.key
  retention: 10
```

### Sauvegarde sur S3
```yaml
---

backup: 
  name: DataSave
  filename: datasave_%d-%m-%Y
  path: 
    - "/data/bdd/"
    - "/data/ressources/"
  dest:
    type: s3
    bucket: saves
    endpoint_url: http://s3.example.com
    access_key: ZLHYXCUJvDOPb5k7yblJ
    secret_key: YaefBJbFrzIpXQ2wfGB4xeG13cFDPedi5J4kK44z
    path: "/backup/"
  encryption:
    key_file: test.key
  retention: 5
```