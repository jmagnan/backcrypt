#!/usr/bin/python

from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
from cryptography.hazmat.primitives import hashes
from Crypto.Random import get_random_bytes
from Crypto.Cipher import AES
from base64 import b64encode
from pathlib import Path
from email.message import EmailMessage
from ftplib import FTP, error_perm
from tqdm import tqdm
import tarfile, yaml, os, glob, datetime, argparse, paramiko, shutil, tempfile, cryptography, base64, struct, secrets, string, smtplib, re, requests, sys

# Variables globales
cdt = datetime.datetime.now()
tempdir = f"{tempfile.gettempdir()}/.backcrypt-" + ''.join(secrets.choice(string.digits + string.ascii_lowercase) for i in range(12))

# Colors
PURPLE = '\033[95m'
CYAN = '\033[96m'
DARKCYAN = '\033[36m'
BLUE = '\033[94m'
GREEN = '\033[32m'
YELLOW = '\033[93m'
RED = '\033[91m'
BOLD = '\033[1m'
UNDERLINE = '\033[4m'
RESET = "\033[0m"

VERSION = 1.3

def CheckUpdate():
    update_lien = "https://gitlab.com/jmagnan/backcrypt/-/raw/master/backcrypt.py"
    ru = requests.get(update_lien, stream=True)
    version_find = re.search(r'VERSION(.*)=(.*)([\d].[\d])', ru.text).group(3)
    if float(version_find) <= float(VERSION):
        return False
    elif float(version_find) > float(VERSION):
        return version_find

CHECK_UPDATE = CheckUpdate()

def Update():
    print(f"{CYAN}Vérification de la mise à jour...{RESET}\n")
    if CHECK_UPDATE is not False:
        print(f"{GREEN}Une mise à jour est disponible: {CHECK_UPDATE}v{RESET}")
        prompt = f"\n{RESET}Mettre à jour ? [o/N]: {RESET}"
        valid = {"oui": True, "o": True, "non": False, "n": False}

        while True:
            sys.stdout.write(prompt)
            choice = input().lower()
            if choice == "":
                print(f"{YELLOW}Aucune mise à jour ne sera installer.{RESET}")
                break
            elif choice in valid:
                if valid[choice]:
                    update_lien = "https://gitlab.com/jmagnan/backcrypt/-/raw/master/backcrypt.py"
                    ru = requests.get(update_lien, stream=True)
                    cl = os.path.dirname(os.path.realpath(__file__))
                    bf = os.path.basename(__file__)
                    shutil.copyfile(f"{cl}/{bf}", f"/tmp/old_{bf}")
                    taille_totale = int(ru.headers.get('content-length', 0))
                    with open(f"{cl}/{bf}", 'wb') as f:
                        with tqdm(total=taille_totale, unit='B', unit_scale=True, desc=f"{BLUE}=> Mise à jour en cour...{RESET} ", ascii=" >=") as pbar:
                            for data in ru.iter_content(1024):
                                f.write(data)
                                pbar.update(len(data))
                    if ru.status_code == 200:
                        print(f"\n{GREEN}Mise à jour effectué.{RESET}")
                    break
                else:
                    print(f"{YELLOW}Aucune mise à jour ne sera installer.{RESET}")
                    break
            else:
                sys.stdout.write(f"{YELLOW}Vous devez répondre \"oui\" ou \"non\" ! (ou \"o\" ou \"n\").\n{RESET}")

    else:
        print(f"{BLUE}Aucune mise à jour disponible !{RESET}")

if CHECK_UPDATE: 
    UPDATE = f"\n{YELLOW}Mise à jour {CHECK_UPDATE}v disponible !"
else:
    UPDATE = ""

BANNER = fr"""{DARKCYAN}
 ____             _     ____                  _   
| __ )  __ _  ___| | __/ ___|_ __ _   _ _ __ | |_ 
|  _ \ / _` |/ __| |/ / |   | '__| | | | '_ \| __|
| |_) | (_| | (__|   <| |___| |  | |_| | |_) | |_ 
|____/ \__,_|\___|_|\_\\____|_|   \__, | .__/ \__|
                                  |___/|_|     
Version: {VERSION}v | https://gitlab.com/jmagnan/backcrypt{UPDATE}
{RESET}"""



class BackupOnType:
    def __init__(self, config):
        self.config = config

    def FilenameFormat(self):
        return f"{cdt.strftime(self.config['filename'].replace(' ', '_'))}"

class BackupOnHost(BackupOnType):
    def __init__(self, config):
        self.config = config

        super().__init__(config)

    def CheckBackupExist(self):
        if os.path.exists(f"{self.config['dest']['path']}/{self.FilenameFormat()}.back") and "force" in self.config:
            if self.config['force'] is True:
                return True
            else:
                print(f"{YELLOW}Un fichier de sauvegarde est déjà présent !{RESET}")
                return False
        else:
            return True

    def MoveBackup(self):
        os.makedirs(self.config['dest']['path'], exist_ok=True)
        src_file = f"{tempdir}/{self.FilenameFormat()}.back"
        dest_file = f"{self.config['dest']['path']}/{self.FilenameFormat()}.back"
        total_size = os.path.getsize(src_file)
        progress = tqdm(total=total_size, unit='B', unit_scale=True, desc=f"{BLUE}=> Transfert local{RESET}  ", ascii=" >=")

        with open(src_file, 'rb') as src, open(dest_file, 'wb') as dest:
            while True:
                buf = src.read(1024 * 1024)
                if not buf:
                    break
                dest.write(buf)
                progress.update(len(buf))
        progress.close()
    def Retention(self):
        try:
            ret = self.config["retention"]
            if ret > 0:
                filename = re.sub(r"%[a-zA-Z%]", "*", f"{self.config['dest']['path']}/{self.config['filename']}.back")
                files = glob.glob(filename)
                files_date = [(file, os.path.getctime(file)) for file in files]
                files_sort = sorted(files_date, key=lambda x: x[1], reverse=True)
                for file in  files_sort[ret:]:
                    print(f"{YELLOW}Fin de la rétention, suppression de {file[0]}{RESET}")
                    os.remove(file[0])
            else:
                print(f"{RED}La rétention ne peut être inférieure à 1 !{RESET}")
        except:
            print(f"{RED}Erreur lors de la rétention dans le dossier: {path}{RESET}")

class BackupOnSSH(BackupOnType):
    def __init__(self, config):
        self.config = config

        super().__init__(config)

    def CheckBackupExist(self):
        ssh = self.LoginToSSH()
        if ssh is not None:
            filename = re.sub(r"%[a-zA-Z%]", "*", f"{self.config['dest']['path']}/{self.config['filename']}.back")
            stdin, stdout, stderr = ssh.exec_command(f"/usr/bin/ls {self.config['dest']['path']}/{self.FilenameFormat()}.back")
            file = stdout.read().decode('utf-8')
        if file is not None and "force" in self.config:
            if self.config['force'] is True:
                return True
            else:
                print(f"{YELLOW}Un fichier de sauvegarde est déjà présent !{RESET}")
                return False
        else:
            return True

    def GetPrivateSSHKey(self):
        key_file_path = os.path.expanduser(self.config['dest']['ssh_key_file'])
        with open(key_file_path, "r") as privatekey:        
            if 'BEGIN RSA' in privatekey:
                return paramiko.RSAKey.from_private_key(key_file_path)
            elif 'BEGIN DSA' in privatekey or 'BEGIN DSS' in privatekey:
                return paramiko.DSSKey.from_private_key(key_file_path)
            elif 'BEGIN ECDSA' in privatekey:
                return paramiko.ECDSAKey.from_private_key(key_file_path)
        return None

    def LoginToSSH(self):
        user = self.config['dest']['login']['user']
        hostname = self.config['dest']['hostname']
        port = self.config['dest']['port'] if 'port' in self.config['dest'] else 22
        password = self.config['dest']['login']['password'] if 'password' in self.config['dest']['login'] else None
        ssh_key_file = self.config['dest']['ssh_key_file'] if 'ssh_key_file' in self.config['dest'] else None

        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        if password is not None and ssh_key_file is None:
            ssh.connect(hostname,port ,username=user,password=password)
            return ssh 
        elif password is not None and ssh_key_file is not None:
            pkey = GetPrivateSSHKey(ssh_key_file)
            ssh.connect(hostname, port, username=user, pkey=pkey, password=password)
            return ssh 
        elif password is None and ssh_key_file is not None:
            pkey = GetPrivateSSHKey(ssh_key_file)
            ssh.connect(hostname, port, username=user, pkey=pkey)
            return ssh 
        return None   

    def SendBackup(self):
        src_file = f"{tempdir}/{self.FilenameFormat()}.back"
        dest_file = f"{self.config['dest']['path']}/{self.FilenameFormat()}.back"
        path = os.path.split(dest_file)[0]
        total_size = os.path.getsize(src_file)
        try:
            ssh = self.LoginToSSH()
            if ssh is not None:
                ssh.exec_command(f"/usr/bin/mkdir -p {path}")

                scp = paramiko.SFTPClient.from_transport(ssh.get_transport())

                progress = tqdm(total=total_size, unit='B', unit_scale=True, desc=f"{BLUE}=> Transfert en SSH{RESET} ", ascii=" >=")

                def progress_callback(self, bytes_transferred):
                    progress.update(bytes_transferred - progress.n)

                scp.put(src_file, dest_file, callback=progress_callback)
            ssh.close()
            progress.close()
        except Exception as Error:
            print(f"{RED}{self.config['name']} - {Error}{RESET}")

    def Retention(self):
        try:
            ret = self.config["retention"]
            if ret > 0:
                s = self.LoginToSSH()
                if s is not None:
                    filename = re.sub(r"%[a-zA-Z%]", "*", f"{self.config['dest']['path']}/{self.config['filename']}.back")
                    stdin, stdout, stderr = s.exec_command(f"/usr/bin/ls {filename}")
                    files = stdout.read().decode('utf-8')
                    files_date = []
                    for file in files.split():
                        stdin, stdout, stderr = s.exec_command(f"/usr/bin/stat -c %W {file}")
                        timestamp = stdout.read().decode('utf-8')
                        files_date.append([file, timestamp])
                    files_sort = sorted(files_date, key=lambda x: x[1], reverse=True)
                    for file in files_sort[ret:]:
                        print(f"{YELLOW}Fin de la rétention, suppression de {file[0]}{RESET}")
                        s.exec_command(f"/usr/bin/rm {file[0]}")
                s.close()
            else:
                print(f"{RED}La rétention ne peut être inférieure à 1 !{RESET}")
        except Exception as Error:
            print(f"{RED}Erreur lors de la rétention: {Error}{RESET}")

class BackupOnS3(BackupOnType):
    def __init__(self, config):
        self.config = config

        super().__init__(config)

    def CheckBackupExist(self):
        s3 = self.LoginToS3()
        bucket = self.config['dest']['bucket']
        file = None
        if s3 is not None:
            objects = s3.list_objects_v2(Bucket=bucket, Prefix=f"{self.config['dest']['path'][1:]}")
            if 'Contents' in objects:
                for obj in objects['Contents']:
                    if obj['Key'] == f"{self.config['dest']['path'][1:]}{self.FilenameFormat()}.back":
                        file = True
                        
        if file is not None and "force" in self.config:
            if self.config['force'] is True:
                return True
            else:
                print(f"{YELLOW}Un fichier de sauvegarde est déjà présent !{RESET}")
                return False
        else:
            return True

    def LoginToS3(self):
        try:
            import boto3
            endpoint_url = self.config['dest']['endpoint_url']
            access_key = self.config['dest']['access_key']
            secret_key = self.config['dest']['secret_key']

            return boto3.client('s3',
                    endpoint_url=endpoint_url,
                    aws_access_key_id=access_key,
                    aws_secret_access_key=secret_key)
            
        except ModuleNotFoundError:
            print(f"{self.config['name']} - Le module \"boto3\" n'est pas installé", log_type="ERROR")
        except Exception as Error:
            print(f"{self.config['name']} - {Error}")

    def SendBackup(self):
        bucket = self.config['dest']['bucket']
        src_file = f"{tempdir}/{self.FilenameFormat()}.back"
        dest_file = f"{self.config['dest']['path']}/{self.FilenameFormat()}.back"

        total_size = os.path.getsize(src_file)
        progress = tqdm(total=total_size, unit='B', unit_scale=True, desc=f"{BLUE}=> Transfert sur S3{RESET} ", ascii=" >=")

        def upload_callback(bytes_amount):
            progress.update(bytes_amount)

        s3 = self.LoginToS3()
        with open(src_file, 'rb') as f:
            s3.upload_fileobj(Fileobj=f, Bucket=bucket,  Key=dest_file, Callback=upload_callback)

        progress.close()

    def Retention(self):
        try:
            ret = self.config["retention"]
            bucket = self.config['dest']['bucket']
            s3 = self.LoginToS3()
            if ret > 0:
                if s3 is not None:
                    files = []
                    regex_pattern = re.sub(r"%[a-zA-Z%]", "(.+)", f"{self.config['dest']['path'][1:]}{self.config['filename']}.back")
                    objects = s3.list_objects_v2(Bucket=bucket, Prefix=f"{self.config['dest']['path'][1:]}")
                    if 'Contents' in objects:
                        for obj in objects['Contents']:
                            if re.match(regex_pattern, obj['Key']):
                                files.append(obj['Key'])
                    files_date = []

                    for file in files:
                        file_metatdata = s3.head_object(Bucket=bucket, Key=file)
                        timestamp = file_metatdata['LastModified'].timestamp()
                        files_date.append([file, timestamp])
                    files_sort = sorted(files_date, key=lambda x: x[1], reverse=True)
                    for file in files_sort[ret:]:
                        print(f"{YELLOW}Fin de la rétention, suppression de {file[0]}{RESET}")
                        s3.delete_object(Bucket=bucket, Key=file[0])
                s3.close()
            else:
                print(f"{RED}La rétention ne peut être inférieure à 1 !{RESET}")
        except Exception as Error:
            print(f"{RED}Erreur lors de la rétention: {Error}{RESET}")

class BackupOnFTP(BackupOnType):
    def __init__(self, config):
        self.config = config

        super().__init__(config)

    def CheckBackupExist(self):
        ftp = self.LoginToFTP()
        file = False
        ftp.cwd(f"{self.config['dest']['path']}")
        files_lst = ftp.nlst()
        for file in files_lst:
            if file == f"{self.config['filename']}.back":
                file = True
        if file is not None and "force" in self.config:
            if self.config['force'] is True:
                return True
            else:
                print(f"{YELLOW}Un fichier de sauvegarde est déjà présent !{RESET}")
                return False
        else:
            return True

    def LoginToFTP(self):
        hostname = self.config['dest']['hostname']
        user = self.config['dest']['login']['user']
        password = self.config['dest']['login']['password']
        port = self.config['dest']['port'] if 'port' in self.config['dest'] else 21
        ftp = FTP()
        ftp.connect(hostname, port)
        ftp.login(user, password)
        return ftp

    def CreatePathFTP(self, ftp, path):
        arbo = path.split('/')
        path_to_create = ""
        for directory in arbo:
            if directory:  
                path_to_create += '/' + directory
                try:
                    ftp.mkd(path_to_create)
                except error_perm as e:
                    if not str(e).startswith('550'): 
                        print(e)
        
    def SendBackup(self):
        try:
            ftp = self.LoginToFTP()
            path = f"{self.config['dest']['path']}"
            self.CreatePathFTP(ftp, path)
            ftp.cwd(path)
            file = f"{tempdir}/{self.FilenameFormat()}.back"
            total_size = os.path.getsize(file)
            progress = tqdm(total=total_size, unit='B', unit_scale=True, desc=f"{BLUE}=> Transfert en FTP{RESET} ", ascii=" >=")
            with open(f"{tempdir}/{self.FilenameFormat()}.back", "rb") as back_file:
                def callback(data):
                    progress.update(len(data))
                ftp.storbinary(f"STOR {self.FilenameFormat()}.back", back_file, 1024, callback)
            ftp.quit()
        except Exception as Error:
            print(f"{RED}Erreur lors de l'envoie sur le serveur FTP: {Error}{RESET}")

    def Retention(self):
        try:
            ret = self.config["retention"]
            if ret > 0:
                ftp = self.LoginToFTP()
                path = f"{self.config['dest']['path']}"
                files = []
                if ftp is not None:
                    regex_pattern = re.sub(r"%[a-zA-Z%]", "(.+)", f"{self.config['filename']}.back")
                    ftp.cwd(path)
                    files_lst = ftp.nlst()
                    for file in files_lst:
                        if re.match(regex_pattern, file):
                            files.append(file)
                    files_date = []
                    for file in files:
                        files_date.append([file, ftp.sendcmd(f"MDTM {file}").split()[1]])
                    files_sort = sorted(files_date, key=lambda x: x[1], reverse=True)
                    for file in files_sort[ret:]:
                        print(f"{YELLOW}Fin de la rétention, suppression de {file[0]}{RESET}")
                        ftp.delete(file[0])
                ftp.quit()
            else:
                print(f"{RED}La rétention ne peut être inférieure à 1 !{RESET}")
        except Exception as Error:
            print(f"{RED}Erreur lors de la rétention: L{Error.__traceback__.tb_lineno} {Error}{RESET}")

class Backup:
    def __init__(self):
        self.config = None

    def TempDirClean(self, create_tempdir = None):
        for old_tempdir in glob.glob(f"{tempfile.gettempdir()}/.backcrypt-*"):
            shutil.rmtree(old_tempdir)
        if create_tempdir:
            os.makedirs(tempdir, exist_ok=True)

    def LoadConfigBackup(self, config_backup):
        self.config = self.CheckConfigIntegrity(config_backup)

    def GetBackUpPath(self):
        if os.access("/etc/",os.W_OK):
            os.makedirs("/etc/backcrypt/backup.d/", exist_ok=True)
        if os.path.exists("/etc/backcrypt/backup.d/"):
            return "/etc/backcrypt/backup.d/"
        elif os.path.exists(f"{os.getcwd()}/backup.d/"):
            return f"{os.getcwd()}/backup.d/"
        else:
            print(f"Le dossier backup.d est manquant !")

    def Run(self):
        if self.config is not None:
            print(f"{GREEN}{BOLD}[{self.config['name']}]{RESET}")
            if self.config['dest']['type'] == "host":
                host = BackupOnHost(self.config)
                if host.CheckBackupExist():
                    self.RunCompress()
                    self.RunEncryption()
                    host.MoveBackup()
                    host.Retention()
            elif self.config['dest']['type'] == "ssh":
                ssh = BackupOnSSH(self.config)
                if ssh.CheckBackupExist():
                    self.RunCompress()
                    self.RunEncryption()
                    ssh.SendBackup()
                    ssh.Retention()
            elif self.config['dest']['type'] == "s3":
                s3 = BackupOnS3(self.config)
                if s3.CheckBackupExist():
                    self.RunCompress()
                    self.RunEncryption()
                    s3.SendBackup()
                    s3.Retention()
            elif self.config['dest']['type'] == "ftp":
                ftp = BackupOnFTP(self.config)
                if ftp.CheckBackupExist():
                    self.RunCompress()
                    self.RunEncryption()
                    ftp.SendBackup()
                    ftp.Retention()

    def RunCompress(self):
        try:
            with tarfile.open(f"{tempdir}/{self.FilenameFormat()}.tar.bz2", "w|bz2") as tf:
                if isinstance(self.config['path'], str):
                    total_size = sum(f.stat().st_size for f in Path(self.config['path']).glob('**/*') if f.is_file())
                    with tqdm(total=total_size, unit='B', unit_scale=True, desc=f"{BLUE}=> Compression{RESET}      ", ascii=" >=") as pbar:
                        for root, _, filenames in os.walk(self.config['path']):
                            for filename in filenames:
                                file_path = os.path.join(root, filename)
                                tf.add(file_path, arcname=os.path.relpath(file_path, self.config['path']))
                                pbar.update(os.path.getsize(file_path))
                elif isinstance(self.config['path'], list):
                    count = 0
                    for path in self.config['path']:
                        count += 1
                        total_size = sum(f.stat().st_size for f in Path(path).glob('**/*') if f.is_file())
                        with tqdm(total=total_size, unit='B', unit_scale=True, desc=f"{BLUE}=> Compression [{count}/{len(self.config['path'])}]{RESET}", ascii=" >=") as pbar:
                            for root, _, filenames in os.walk(path):
                                for filename in filenames:
                                    file_path = os.path.join(root, filename)
                                    tf.add(file_path, arcname=os.path.relpath(file_path, path))
                                    pbar.update(os.path.getsize(file_path))
        except Exception as Error:
            print(f"{RED}{self.config['name']} - {Error}{RESET}")

    def GetKeysPath(self):
        if os.access("/etc/",os.W_OK):
            os.makedirs("/etc/backcrypt/keys.d/", exist_ok=True)

        if os.path.exists("/etc/backcrypt/keys.d/"):
            return "/etc/backcrypt/keys.d/"
        elif os.path.exists(f"{os.getcwd()}/keys.d/"):
            return f"{os.getcwd()}/keys.d/"
        else:
            print(f"{RED}Le dossier keys.d est manquant !")

    def GetKeyFromFile(self, keyfile):
        keys_path = self.GetKeysPath()
        if os.path.exists(f"{keys_path}/{keyfile}"):
            with open(f"{keys_path}/{keyfile}", "rb") as key_file:
                return base64.b64decode(key_file.read())
        else:
            key = get_random_bytes(32)
            with open(f"{keys_path}/{keyfile}","wb", 400) as key_file:
                key_file.write(base64.b64encode(key))
            return key

    def SetPasswordEncryption(self, password):
        self.password = password

    def RunEncryption(self, password = None):
        key = self.GetKeyFromFile(self.config['encryption']['key_file'])
        try:
            chunksize = 24*1024
            iv = get_random_bytes(16)
            cipher = AES.new(key, AES.MODE_CBC, iv)
            filesize = os.path.getsize(f"{tempdir}/{self.FilenameFormat()}.tar.bz2")
            with open(f"{tempdir}/{self.FilenameFormat()}.tar.bz2", "rb") as tarbz2_file, open(f"{tempdir}/{self.FilenameFormat()}.back", "wb") as encrypted_file:
                with tqdm(total=filesize, unit='B', unit_scale=True, desc=f"{BLUE}=> Chiffrement{RESET}      ", ascii=" >=") as pbar:
                    encrypted_file.write(struct.pack('<Q', filesize))
                    encrypted_file.write(iv)

                    while True:
                        chunk = tarbz2_file.read(chunksize)
                        pbar.update(len(chunk))
                        if len(chunk) == 0:
                            break
                        elif len(chunk) % 16 != 0:
                            chunk += ' '.encode() * (16 - len(chunk) % 16)
                        encrypted_file.write(cipher.encrypt(chunk))
                    
        except Exception as Error:
            print(f"{RED}{self.config['name']} - {Error}{RESET}")

    def Decrypt(self, backup_file, key_file):
        if not os.path.isfile(backup_file):
            print(f"{RED}Sauvegarde introuvable !{RESET}")
        elif not os.path.isfile(key_file):
            print(f"{RED}Fichier de clé introuvable !{RESET}")
        else:
            with open(key_file, 'rb') as key:
                key_read = base64.b64decode(key.read())
                
            chunksize=24*1024
            output_file = backup_file.split("/")[-1].replace(".back",".tar.bz2")
            filesize = os.path.getsize(backup_file)

            with open(backup_file, 'rb') as encrypted_file, open(output_file, 'wb') as tarbz2_file:
                with tqdm(total=filesize, unit='B', unit_scale=True, desc=f"{BLUE}Déchiffrement{RESET}", ascii=" >=") as pbar:
                    origsize = struct.unpack('<Q', encrypted_file.read(struct.calcsize('Q')))[0]
                    iv = encrypted_file.read(16)
                    cipher = AES.new(key_read, AES.MODE_CBC, iv)
                    while True:
                        chunk = encrypted_file.read(chunksize)
                        if len(chunk) == 0:
                            break
                        tarbz2_file.write(cipher.decrypt(chunk))
                        pbar.update(len(chunk))
                    tarbz2_file.truncate(origsize)

    def CheckConfigIntegrity(self, config_backup):
        backup_config_path = self.GetBackUpPath()
        if os.path.isfile(f"{backup_config_path}/{config_backup}.yml"):
            with open(f"{backup_config_path}/{config_backup}.yml", mode='r', encoding="utf-8") as config:
                try:
                    backup_config = yaml.safe_load(config)
                except:
                    print(f"{config_backup}.yml - Erreur de syntaxe lors du chargement de la configuration !")
                    return None
            if backup_config is None:
                print(f"{config_backup}.yml - Clé manquante \"backup\"")
                return None

            if 'name' not in backup_config['backup']:
                print(f"{config_backup}.yml - Clé manquante \"name\"")
                return None
            elif backup_config['backup']['name'] is None:
                print(f"{config_backup}.yml - Valeur manquante \"name\"")
                return None

            if 'filename' not in backup_config['backup']:
                print(f"{config_backup}.yml - Clé manquante \"filename\"")
                return None
            elif backup_config['backup']['filename'] is None:
                print(f"{config_backup}.yml - Valeur manquante \"filename\"")
                return None

            if 'path' not in backup_config['backup']:
                print(f"{config_backup}.yml - Clé manquante \"path\"")
                return None
            elif backup_config['backup']['path'] is None:
                print(f"{config_backup}.yml - Valeur manquante \"path\"")
                return None

            if 'dest' not in backup_config['backup']:
                print(f"{config_backup}.yml - Clé manquante \"dest\"")
                return None
            elif backup_config['backup']['dest'] is None:
                print(f"{config_backup}.yml - La clé \"dest\" valeur ne doit pas contenir ce type de valeur")
                return None

            if 'encryption' not in backup_config['backup']:
                print(f"{config_backup}.yml - Clé manquante \"encryption\"")
                return None
            elif backup_config['backup']['encryption'] is None:
                print(f"{config_backup}.yml - La clé \"encryption\" valeur ne doit pas contenir ce type de valeur")
                return None
            else:
                if 'key_file' not in backup_config['backup']['encryption']:
                    print(f"{config_backup}.yml - Clé manquante \"encryption\"")
                    return None
                elif backup_config['backup']['encryption']['key_file'] is None:
                    print(f"{config_backup}.yml - Valeur manquante \"key_file\"")
                    return None

            if 'path' not in backup_config['backup']['dest']:
                print(f"{config_backup}.yml - Clé manquante \"path\"")
                return None
            elif backup_config['backup']['dest']['path'] is None:
                print(f"{config_backup}.yml - Valeur manquante \"path\"")
                return None

            if 'force' in backup_config['backup']:       
                if backup_config['backup']['force'] is None:
                    print(f"{config_backup}.yml - Valeur manquante \"force\"")
                    return None
                elif backup_config['backup']['force'] is not True and backup_config['backup']['force'] is not False:
                    print(f"{config_backup}.yml - La valeur \"force\" doit être \"yes\" ou \"no\"")
                    return None

            if 'retention' in backup_config['backup']:       
                if backup_config['backup']['retention'] is None:
                    print(f"{config_backup}.yml - Valeur manquante \"force\"")
                    return None
                elif not isinstance(backup_config['backup']['retention'], int):
                    print(f"{config_backup}.yml - La valeur \"retention\" doit être un nombre")
                    return None

            if 'type' not in backup_config['backup']['dest']:
                print(f"{config_backup}.yml - Clé manquante \"type\"")
                return None
            elif backup_config['backup']['dest']['type'] is None:
                print(f"{config_backup}.yml - Valeur manquante \"type\"")
                return None
            else:
                if backup_config['backup']['dest']['type'] == "host":
                    pass
                elif backup_config['backup']['dest']['type'] == "ssh":
                    if 'login' not in backup_config['backup']['dest']:
                        print(f"{config_backup}.yml - Clé manquante \"login\"")
                        return None
                    elif backup_config['backup']['dest']['login'] is None:
                        print(f"{config_backup}.yml - Valeur manquante \"login\"")
                        return None
                    if 'hostname' not in backup_config['backup']['dest']:
                        print(f"{config_backup}.yml - Clé manquante \"hostname\"")
                        return None
                    elif backup_config['backup']['dest']['hostname'] is None:
                        print(f"{config_backup}.yml - Valeur manquante \"hostname\"")
                        return None
                    if 'port' in backup_config['backup']['dest']:       
                        if backup_config['backup']['dest']['port'] is None:
                            print(f"{config_backup}.yml - Valeur manquante \"port\"")
                            return None
                    if 'ssh_key_file' in backup_config['backup']['dest']:       
                        if backup_config['backup']['dest']['ssh_key_file'] is None:
                            print(f"{config_backup}.yml - Valeur manquante \"ssh_key_file\"")
                            return None
                        elif os.path.isfile(os.path.expanduser(backup_config['backup']['dest']['ssh_key_file'])) is False:
                            print(f"{config_backup}.yml - Le fichier de la clé SSH n'est pas présent à cet emplacement: {backup_config['backup']['dest']['ssh_key_file']}")
                            return None
                elif backup_config['backup']['dest']['type'] == "s3":
                    if 'bucket' not in backup_config['backup']['dest']:
                        print(f"{config_backup}.yml - Clé manquante \"bucket\"")
                        return None
                    elif backup_config['backup']['dest']['bucket'] is None:
                        print(f"{config_backup}.yml - Valeur manquante \"bucket\"")
                        return None
                    if 'endpoint_url' not in backup_config['backup']['dest']:
                        print(f"{config_backup}.yml - Clé manquante \"endpoint_url\"")
                        return None
                    elif backup_config['backup']['dest']['endpoint_url'] is None:
                        print(f"{config_backup}.yml - Valeur manquante \"endpoint_url\"")
                        return None
                    if 'access_key' not in backup_config['backup']['dest']:
                        print(f"{config_backup}.yml - Clé manquante \"access_key\"")
                        return None
                    elif backup_config['backup']['dest']['access_key'] is None:
                        print(f"{config_backup}.yml - Valeur manquante \"access_key\"")
                        return None
                    if 'secret_key' not in backup_config['backup']['dest']:
                        print(f"{config_backup}.yml - Clé manquante \"secret_key\"")
                        return None
                    elif backup_config['backup']['dest']['secret_key'] is None:
                        print(f"{config_backup}.yml - Valeur manquante \"secret_key\"")
                        return None
                elif backup_config['backup']['dest']['type'] == "ftp":
                    pass
                else:
                    print("Ce type n'est pas disponible")
                    return None
            return backup_config['backup']
        else:
            return None

    def ListConfig(self):
        backup_path = self.GetBackUpPath()
        list_config = []
        print ("{:30} {:20} {:30} {:30} {:60}".format(f"{BLUE}Nom{RESET}",f"{BLUE}Type{RESET}", f"{BLUE}Configuration{RESET}",f"{BLUE}Destination{RESET}",f"{BLUE}Sources{RESET}"))
        for config_file in glob.glob(f"{backup_path}/**/*.yml", recursive=True):
            self.LoadConfigBackup(config_file.split("backup.d/")[1].replace(".yml", ""))
            list_config.append([self.config['name'],self.config['dest']['type'], config_file.split("backup.d/")[1].replace(".yml", ""),self.config['dest']['path'],self.config['path']])
        for row in list_config:
            print("{:30} {:20} {:30} {:30} {:60}".format(f"{GREEN}{row[0]}{RESET}", f"{GREEN}{row[1]}{RESET}", f"{GREEN}{row[2]}{RESET}", f"{GREEN}{row[3]}{RESET}", f"{GREEN}{row[4] if isinstance(row[4], str) else str(', '.join(row[4]))}{RESET}"))

    def FilenameFormat(self):
        return f"{cdt.strftime(self.config['filename'].replace(' ', '_'))}"

if __name__ == '__main__':
    print(BANNER)

    parser = argparse.ArgumentParser(description='Système de sauvegarde avec chiffrement')

    parser.add_argument('-l', '--list-config', action='store_true', help="Liste les fichiers de configuration des sauvegardes")
    parser.add_argument('-t', '--test-config', help="Vérification de la configuration d'un fichier de sauvegarde")
    parser.add_argument('-v', '--version', action='store_true', help="Affiche la version de Backcrypt")
    parser.add_argument('-u', '--update', action='store_true', help="Vérifie et fais les msie à jour") 

    parser_save = parser.add_argument_group('Sauvegardes')
    parser_save.add_argument('-a', '--save-all', action='store_true', help="Lance toutes les tâches de sauvegardes") 
    parser_save.add_argument('-s', '--save', nargs='+', help="Lance une tâche de sauvegarde")

    parser_decrypt = parser.add_argument_group("Restauration d'une sauvegardes")
    parser_decrypt.add_argument('-d', '--decrypt-file', help="Déchiffre une sauvegarde")
    parser_decrypt.add_argument('-k', '--key-file', help="Fichier de la clé de sauvegarde")

    args = parser.parse_args()

    backup = Backup()
    backup.TempDirClean(create_tempdir=True)

    if args.save:
        if len(args.save) == 1:
            backup.LoadConfigBackup(args.save[0])
            backup.Run()
        elif len(args.save) > 1:
            for index, save in enumerate(args.save):
                backup.LoadConfigBackup(save)
                if index != 0 and index != len(save) - 1:
                    print("")
                backup.Run()
    elif args.list_config:
        backup.ListConfig()
    elif args.update:
        Update()
    elif args.decrypt_file and args.key_file:
        backup.Decrypt(args.decrypt_file, args.key_file)
    elif args.test_config:
        if backup.CheckConfigIntegrity(args.test_config) is not None:
            print(f"{GREEN}Vérifications ok !{RESET}")

    if args.version:
        print(f"{BLUE}Version: {VERSION}{RESET}")

    backup.TempDirClean()