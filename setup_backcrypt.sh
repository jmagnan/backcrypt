#!/bin/bash

if [[ $EUID -ne 0 ]]; then
    echo "Le script doit être lancé en tant que root !" 1>&2
    exit 100
fi

# Download and Install
BIN_SRC="https://gitlab.com/jmagnan/backcrypt/-/raw/master/backcrypt.py"

# Packages
declare -A osInfo;
osInfo[/etc/redhat-release]=yum
osInfo[/etc/arch-release]=pacman
osInfo[/etc/gentoo-release]=emerge
osInfo[/etc/SuSE-release]=zypp
osInfo[/etc/debian_version]=apt-get
osInfo[/etc/alpine-release]=apk

YUM_DEP="python3-paramiko python3-tqdm python3-cryptography python3-devel"
PACMAN_DEP="python-paramiko python-tqdm python-cryptography"

### START ###

cat << "EOF"
 ____             _     ____                  _   
| __ )  __ _  ___| | __/ ___|_ __ _   _ _ __ | |_ 
|  _ \ / _` |/ __| |/ / |   | '__| | | | '_ \| __|
| |_) | (_| | (__|   <| |___| |  | |_| | |_) | |_ 
|____/ \__,_|\___|_|\_\\____|_|   \__, | .__/ \__|
                                  |___/|_|     
Setup version: 1.0 | https://gitlab.com/jmagnan/backcrypt
EOF

echo -e "\n[+] Téléchargement et installation"

curl -s $BIN_SRC > /usr/local/bin/backcrypt
chmod +x /usr/local/bin/backcrypt

if [ "$?" == 0 ]; then
	echo "OK!"
else
	echo "Erreurs lors du téléchargement !"
	exit 1
fi

echo -e "\n[+] Installation des packages nécessaire"

for f in ${!osInfo[@]};do
    if [[ -f $f ]];then
		case "$manager" in
			"yum")
			yum install -y $YUM_DEP >> /dev/null 2>&1
			;;
			"pacman")
			pacman -Sy $PACMAN_DEP --needed --noconfirm >> /dev/null 2>&1
			;;
		esac

		if [ "$?" == 0 ]; then
			echo "OK!"
		else
			echo "Erreurs lors de l'installation des dépendances, elles doivents être installés manuellement !"
			exit 1
		fi
    fi
done

echo -e "\nInstallation terminé, des exemples de configuration sont disponible sur le dépot GitLab"
